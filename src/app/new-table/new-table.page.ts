import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Component({
  selector: 'app-new-table',
  templateUrl: './new-table.page.html',
  styleUrls: ['./new-table.page.scss'],
})
export class NewTablePage implements OnInit {

  constructor( public afs: AngularFirestore
    ) { }

  ngOnInit() {
  }

}
